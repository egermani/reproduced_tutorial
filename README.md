# reproduced_tutorial

This repository contains instruction files and notebooks that use nipype to reproduce some example workflows. 

These workflows reproduce example workflows that can be found on [SPM manual](https://www.fil.ion.ucl.ac.uk/spm/doc/manual.pdf) and on [FSL course](https://fsl.fmrib.ox.ac.uk/fslcourse/lectures/practicals/feat1/index.html).

These were adapted on two datasets downloadable : 
* Tutorial_data : [here](https://openneuro.org/datasets/ds000114/versions/1.0.1) ; 
* Fluency task : data can be downloaded using a pre-written script you can find [here](https://fsl.fmrib.ox.ac.uk/fslcourse/#Data) (repository to download : fmri1, section 3 on FEAT1). Data can also be downloaded using a terminal : 
	* Using Windows or linux, use : ```wget -c http://fsl.fmrib.ox.ac.uk/fslcourse/downloads/fmri1.tar.gz```
	* Using Mac, use : ```curl -# -O -C - http://fsl.fmrib.ox.ac.uk/fslcourse/downloads/fmri1.tar.gz```


To use the notebooks, create a docker container using the instructions in the file and use the parent repository of **"analytic_variability_fmri"** as a volume. 

The dataset should be downloaded in a repository named **"fluency_task"** or **"tutorial_data"** depending on which dataset you use. This repositories should be in the directory **"data"** inside this repository. 

Otherwise, if you want to use different paths, you need to update these paths on the notebooks. 
